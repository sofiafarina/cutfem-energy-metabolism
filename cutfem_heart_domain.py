"""
We solve 5 chemical reaction that describes the Energy Metabolism of a cell:

HXK: A+2B -> 2C + 2D
PYRK: D + 2C -> 2B + E
LDH: E -> F
Mito: E -> 28 B
act: B -> C

where in particular:
A = [GLC]
B = [ATP]
C = [ADP]
D = [GLY]
E = [PYR]
F = [LAC]

We indicate with K1,K2,K3,K4,K5 the rate constants of the reactions.

The enzyme location and activating of the reaction is represented by a Gaussian function
G location of HXK
G2 location of PYRK
G3 location of LDH
G4 location of Mito
G5 location of act

the PDEs system:

A_t = D_a * A_xx - K1 * AB^2 * G + f
B_t = D_b * B_xx - 2K1 * AB^2 * G + 2K2 * DC^2 * G2 +28 K4 * E * G4 - K5 * B * G5
C_t = D_c * C_xx + 2K1 * AB^2 * G - K2 * DC^2 * G2 + K5 * B * G5
D_t = D_d * D_xx + 2K1 * AB^2 * G - K2 * DC^2 * G2
E_t = D_e * E_xx +  K2 * DC^2 * G2 - K3 * E * G3 - K4 * E * G4
F_t = D_f * F_xx + K3 * E * G3

+ Pure Neumann Boundary Condition
+ Initial Condition at t=0 of the concentrations

"""

from dolfin import *
from cutfem import *
parameters["form_compiler"]["no-evaluate_basis_derivatives"] = False


T = 10.0  # final time
num_step = 300  # number of time step
dt = T / num_step

# Create mesh
N = 200
bg_mesh = RectangleMesh(-1, -1, 1., 2., N, N)

# level set
level_set = Expression(
    'x[0] < 0. ? pow(x[1]-sqrt(-x[0]),2)-1 + pow(x[0],2) : pow(x[1]-sqrt(x[0]),2)-1 + pow(x[0],2)', degree=2)

# Define ficitious domain

mesh = CutFEMTools_fictitious_domain_mesh(bg_mesh, level_set, 0, 0)

# Define Normal mesh
n = FacetNormal(mesh)
h = CellSize(mesh)

# penalty parameter
gamma = 0.1

# Compute normals
Vn = VectorFunctionSpace(mesh, "CG", 1)
Fn = FunctionSpace(mesh, "CG", 2)
n_K = TrialFunction(Vn)
vn = TestFunction(Vn)

ls = Function(Fn)
ls.interpolate(level_set)

norm = sqrt(dot(grad(ls), grad(ls)))
an = norm*dot(n_K, vn)*dx
Ln = dot(grad(ls), vn)*dx

An = assemble(an)
bn = assemble(Ln)
n_K = Function(Vn)

solve(An, n_K.vector(), bn)

file = File("./results/n_K.pvd")
file << n_K

# Compute Mesh -Levelset intersection and corresponding marker
mesh_cutter = MeshCutter(mesh, level_set)

# Save solution in VTK format
file = File("./results/domain_marker.pvd")
file << mesh_cutter.domain_marker()

file = File("./results/interface_cells.pvd")
file << mesh_cutter.interface_cells()

file = File("./results/cut_cells.pvd")
file << mesh_cutter.cut_cells(0)

file = File("./results/interior_facets.pvd")
file << mesh_cutter.interior_facet_marker(0)

# Define new measures associated with the interior domains and facets

dx = Measure("dx")[mesh_cutter.domain_marker()]
dS = Measure("dS")[mesh_cutter.interior_facet_marker(0)]
dxq = dc(0, metadata={"num_cells": 1})
dsq = dc(1, metadata={"num_cells": 1})

# Measure Inside the level set + the cutcell  Inside the levelset
dxc = dx(0) + dxq

# Define the Space
T = FunctionSpace(mesh, "P", 1)
V0 = MixedFunctionSpace([T, T, T, T, T, T])

# Define test functions
(v_1, v_2, v_3, v_4, v_5, v_6) = TestFunctions(V0)

# Define Trial functions
u = Function(V0)

# Define the initial condition
a_0 = 0.
b_0 = 1.
c_0 = 1.
d_0 = 0.
e_0 = 0.
f_0 = 0.

u_0 = Expression(('a_0', 'b_0', 'c_0', 'd_0', 'e_0', 'f_0'),
                 a_0=a_0, b_0=b_0, c_0=c_0, d_0=d_0, e_0=e_0, f_0=f_0, degree=1)
u_n = project(u_0, V0)

a, b, c, d, e, f = split(u)
a_n, b_n, c_n, d_n, e_n, f_n = split(u_n)

# Define the Gaussian function indicating the chemical reaction's location

g_hxk = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                   x0=0.1, y0=-0.5, sigma=.1, degree=2)

# Define the Gaussian function indicating where PYRK reaction take place
g_pyrk = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                    x0=-0.3, y0=0.0, sigma=.1, degree=2)

# Define the Gaussian function indicating where PYRK reaction take place
g_ldh = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                   x0=-0.5, y0=0.5, sigma=.1, degree=2)

# Define the Gaussian function indicating where PYRK reaction take place
g_mito = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                    x0=0.5, y0=0.7, sigma=.1, degree=2)

# Cellular activity location
g_act = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                   x0=0.0, y0=0.9, sigma=.1, degree=2)

# Define Constant
k = Constant(dt)

# Rate constants

K_hxk = Constant(10.)
K_pyrk = Constant(10.)
K_ldh = Constant(10.)
K_Mito = Constant(10.)
K_act = Constant(10.)

# Diffusion constants

D_glc = Constant(1.)
D_atp = Constant(1.)
D_adp = Constant(1.)
D_gly = Constant(1.)
D_pyr = Constant(1.)
D_lac = Constant(1.)


# Time stepping
t = [0.0]

# Define Source term
influx = 1.e2
f_glc = Expression('(pow(x[0],2)+pow(x[1]+1,2)) < (r * r) && t<= 1.0 ? influx : 0',
                   t=t[0], influx=influx, r=0.3, degree=1)

# Define the variational problem

F = ((a - a_n) / k) * v_1 * dxc \
    + D_glc * dot(grad(a), grad(v_1)) * dxc + K_hxk * a * b**2 * v_1 * g_hxk * dxc \
    + ((b - b_n) / k) * v_2 * dxc  \
    + D_atp * dot(grad(b), grad(v_2)) * dxc + 2 * K_hxk * a * b**2 * v_2 * g_hxk * dxc - 2 * K_pyrk * d * c**2 * v_2 * g_pyrk * dxc - 28 * K_Mito * e * v_2 * g_mito * dxc + K_act * b * v_2 * g_act * dxc\
    + ((c - c_n) / k) * v_3 * dxc \
    + D_adp * dot(grad(c), grad(v_3)) * dxc - 2 * K_hxk * a * b**2 * v_3 * g_hxk * dxc + 2 * K_pyrk * d * c**2 * v_3 * g_pyrk * dxc - K_act * b * v_3 * g_act * dxc\
    + ((d - d_n) / k) * v_4 * dxc\
    + D_gly * dot(grad(d), grad(v_4)) * dxc - 2 * K_hxk * a * b**2 * v_4 * g_hxk * dxc + K_pyrk * d * c**2 * v_4 * g_pyrk * dxc\
    + ((e - e_n) / k) * v_5 * dxc\
    + D_pyr * dot(grad(e), grad(v_5)) * dxc - K_pyrk * d * c**2 * v_5 * g_pyrk * dxc + K_ldh * e * v_5 * g_ldh * dxc + K_Mito * e * v_5 * g_mito * dxc\
    + ((f - f_n) / k) * v_6 * dxc\
    + D_lac * dot(grad(f), grad(v_6)) * dxc - K_ldh * e * v_6 * g_ldh * dxc\
    - f_glc * v_1 * dxc


F += avg(gamma) * avg(h) * D_glc * dot(jump(grad(a), n), jump(grad(v_1), n)) * dS(1) + avg(gamma) * avg(h) * D_atp * dot(jump(grad(b), n), jump(grad(v_2), n)) * dS(1)\
    + avg(gamma) * avg(h) * D_adp * dot(jump(grad(c), n), jump(grad(v_3), n)) * dS(1) + avg(gamma) * avg(h) * D_gly * dot(jump(grad(d), n), jump(grad(v_4), n)) * dS(1)\
    + avg(gamma) * avg(h) * D_pyr * dot(jump(grad(e), n), jump(grad(v_5), n)) * dS(1) + \
    avg(gamma) * avg(h) * D_lac * \
    dot(jump(grad(f), n), jump(grad(v_6), n)) * dS(1)

# Derivative of F

J = derivative(F, u)

# Fictitious domain

composite_mesh = CompositeMesh()
composite_mesh.add(mesh)

V = CompositeFunctionSpace(composite_mesh)
V.add(V0)
V.build()

# Constrain dofs outside
FidoTools_compute_constrained_dofs(V, mesh_cutter)

a = FidoForm(V, V)
form_a = create_dolfin_form(J)
a.add(form_a, mesh_cutter)

L = FidoForm(V)
form_L = create_dolfin_form(F)
L.add(form_L, mesh_cutter)

# Create VTK files for visualization output

vtkfile_a = File('results/glc/a.pvd')
vtkfile_b = File('results/atp/b.pvd')
vtkfile_c = File('results/adp/c.pvd')
vtkfile_d = File('results/gly/d.pvd')
vtkfile_e = File('results/pyr/e.pvd')
vtkfile_f = File('results/lac/f.pvd')

# Space for the solutions
cutmesh0 = CutFEMTools_physical_domain_mesh(
    mesh, mesh_cutter.cut_cells(0), mesh_cutter.domain_marker(), 0)
V0Phys = FunctionSpace(cutmesh0, "CG", 1)
a_inter = Function(V0Phys)
b_inter = Function(V0Phys)
c_inter = Function(V0Phys)
d_inter = Function(V0Phys)
e_inter = Function(V0Phys)
f_inter = Function(V0Phys)

# Parameter for solver
Nmax = 50
toll_a = 1.e-10

for i in range(num_step):

    n = 1
    while n < Nmax:

        A = composite_assemble(a)
        b = composite_assemble(L)

        uc = CompositeFunction(V)

        solve(A, uc.vector(), -b, 'mumps')

        u.vector().axpy(1.0, uc.part(0).vector())

        if uc.part(0).vector().norm("l2") < toll_a:
            break
        else:
            n += 1
    _a, _b, _c, _d, _e, _f = u.split()

    # save a
    a_inter.interpolate(_a)

    # save b
    b_inter.interpolate(_b)

    # save c

    c_inter.interpolate(_c)

    # save d

    d_inter.interpolate(_d)

    # save e

    e_inter.interpolate(_e)
    # save e

    f_inter.interpolate(_f)

    vtkfile_a << (a_inter, t[0])
    vtkfile_b << (b_inter, t[0])
    vtkfile_c << (c_inter, t[0])
    vtkfile_d << (d_inter, t[0])
    vtkfile_e << (e_inter, t[0])
    vtkfile_f << (f_inter, t[0])

    # Update the solution
    u_n._assign(u)

    # Update the time step
    t[0] = t[0] + dt

    # update the source term
    f_glc.t = t[0]
