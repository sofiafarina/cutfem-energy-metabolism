"""
We solve 5 chemical reaction that describes the Energy Metabolism of a cell:

HXK: A+2B -> 2C + 2D
PYRK: D + 2C -> 2B + E
LDH: E -> F
Mito: E -> 28 B
act: B -> C

where in particular:
A = [GLC]
B = [ATP]
C = [ADP]
D = [GLY]
E = [PYR]
F = [LAC]

We indicate with K1,K2,K3,K4,K5 the rate constants of the reactions.

The enzyme location and activating of the reaction is represented by a Gaussian function
G location of HXK
G2 location of PYRK
G3 location of LDH
G4 location of Mito
G5 location of act

the PDEs system:

A_t = D_a * A_xx - K1 * AB^2 * G + f
B_t = D_b * B_xx - 2K1 * AB^2 * G + 2K2 * DC^2 * G2 +28 K4 * E * G4 - K5 * B * G5
C_t = D_c * C_xx + 2K1 * AB^2 * G - K2 * DC^2 * G2 + K5 * B * G5
D_t = D_d * D_xx + 2K1 * AB^2 * G - K2 * DC^2 * G2
E_t = D_e * E_xx +  K2 * DC^2 * G2 - K3 * E * G3 - K4 * E * G4
F_t = D_f * F_xx + K3 * E * G3

+ Pure Neumann Boundary Condition
+ Initial Condition at t=0 of the concentrations

"""

from dolfin import *
from mshr import *

T = 10.  # final time
num_step = 100  # number of time step
dt = T / num_step


# Create mesh
N = 200
channel = Circle(Point(4., 3.), 5)
mesh = generate_mesh(channel, N)


# Finite Element space for the concentration
P1 = FiniteElement('P', triangle, 1)
element = MixedElement([P1, P1, P1, P1, P1, P1])
V = FunctionSpace(mesh, element)

# Define test functions
v_1, v_2, v_3, v_4, v_5, v_6 = TestFunctions(V)

# Define Trial function
u = Function(V)

# Define the initial condition of the concetration

a_0 = 0.
b_0 = 1.
c_0 = 1.
d_0 = 0.
e_0 = 0.
f_0 = 0.

u_0 = Expression(('a_0', 'b_0', 'c_0', 'd_0', 'e_0', 'f_0'),
                 a_0=a_0, b_0=b_0, c_0=c_0, d_0=d_0, e_0=e_0, f_0=f_0, degree=1)
u_n = project(u_0, V)


a, b, c, d, e, f = split(u)
a_n, b_n, c_n, d_n, e_n, f_n = split(u_n)

# Define the Gaussian function indicating where HXK reaction take place
g_hxk = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                   x0=0.5, y0=2.0, sigma=.1, degree=2)

# Define the Gaussian function indicating where PYRK reaction take place
g_pyrk = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                    x0=1.1, y0=1.2, sigma=.1, degree=2)

# Define the Gaussian function indicating where PYRK reaction take place
g_ldh = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                   x0=4., y0=5.0, sigma=.1, degree=2)

# Define the Gaussian function indicating where PYRK reaction take place
g_mito = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                    x0=4.0, y0=7.5, sigma=.1, degree=2)

# Cellular activity location
g_act = Expression("1./sqrt(pi*2*sigma*sigma)*exp(-((x[0]-x0)*(x[0]-x0)+(x[1]-y0)*(x[1]-y0))/(2*sigma*sigma))",
                   x0=6.0, y0=6.5, sigma=.1, degree=2)


# Define Constant
k = Constant(dt)

K_hxk = Constant(10.)
K_pyrk = Constant(10.)
K_ldh = Constant(10.)
K_Mito = Constant(10.)
K_act = Constant(10.)

# Diffusion constant
D_glc = Constant(1.)
D_atp = Constant(1.)
D_adp = Constant(1.)
D_gly = Constant(1.)
D_pyr = Constant(1.)
D_lac = Constant(1.)


# Time stepping
t = [0.0]

# #Define Source term
influx = 1.e2
f_glc = Expression('(pow(x[0],2)+pow(x[1],2)) < (r * r) && t<= 1.0 ? influx : 0',
                   t=t[0], influx=influx, r=0.3, degree=1)

# Define the variational problem

F = ((a - a_n) / k) * v_1 * dx \
    + D_glc * dot(grad(a), grad(v_1)) * dx + K_hxk * a * b**2 * v_1 * g_hxk * dx \
    + ((b - b_n) / k) * v_2 * dx  \
    + D_atp * dot(grad(b), grad(v_2)) * dx + 2 * K_hxk * a * b**2 * v_2 * g_hxk * dx - 2 * K_pyrk * d * c**2 * v_2 * g_pyrk * dx - 28 * K_Mito * e * v_2 * g_mito * dx + K_act * b * v_2 * g_act * dx\
    + ((c - c_n) / k) * v_3 * dx \
    + D_adp * dot(grad(c), grad(v_3)) * dx - 2 * K_hxk * a * b**2 * v_3 * g_hxk * dx + 2 * K_pyrk * d * c**2 * v_3 * g_pyrk * dx - K_act * b * v_3 * g_act * dx\
    + ((d - d_n) / k) * v_4 * dx\
    + D_gly * dot(grad(d), grad(v_4)) * dx - 2 * K_hxk * a * b**2 * v_4 * g_hxk * dx + K_pyrk * d * c**2 * v_4 * g_pyrk * dx\
    + ((e - e_n) / k) * v_5 * dx\
    + D_pyr * dot(grad(e), grad(v_5)) * dx - K_pyrk * d * c**2 * v_5 * g_pyrk * dx + K_ldh * e * v_5 * g_ldh * dx + K_Mito * e * v_5 * g_mito * dx\
    + ((f - f_n) / k) * v_6 * dx\
    + D_lac * dot(grad(f), grad(v_6)) * dx - K_ldh * e * v_6 * g_ldh * dx\
    - f_glc * v_1 * dx

# Create VTK files for visualization output
vtkfile_a = File('results/glc/a.pvd')
vtkfile_b = File('results/atp/b.pvd')
vtkfile_c = File('results/adp/c.pvd')
vtkfile_d = File('results/gly/d.pvd')
vtkfile_e = File('results/pyr/e.pvd')
vtkfile_f = File('results/lac/f.pvd')


for n in range(num_step):
    # Solve the variational form for time step
    solve(F == 0, u)

    # Save solution to file (VTK)
    _a, _b, _c, _d, _e, _f = u.split()

    vtkfile_a << (_a, t[0])
    vtkfile_b << (_b, t[0])
    vtkfile_c << (_c, t[0])
    vtkfile_d << (_d, t[0])
    vtkfile_e << (_e, t[0])
    vtkfile_f << (_f, t[0])

    # Update the previous solution
    u_n.assign(u)

    # Update time
    t[0] = t[0] + dt

    # Update source term
    f_glc.t = t[0]
